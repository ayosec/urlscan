use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io;

mod urls {
    include!(concat!(env!("OUT_DIR"), "/urls.rs"));
}

use urls::url;

fn extract_from_lines<R: BufRead>(reader: &mut R) {

    for line in reader.lines() {
        if let Ok(line) = line {
            let mut tail = &line[..];
            while let Ok((matched, t)) = url(tail) {
                println!("{}", matched);
                tail = t;
            }
        }
    }

}

fn main() {

    let args = env::args();

    if args.len() == 1 {
        let stdin = io::stdin();
        extract_from_lines(&mut stdin.lock());
    } else {
        for arg in args.skip(1) {
            if let Ok(file) = File::open(arg) {
                let mut reader = io::BufReader::new(file);
                extract_from_lines(&mut reader);
            }
        }
    }

}
